package com.example.cahngsch.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cahngsch.Adapter.SachAdapter;
import com.example.cahngsch.Adapter.TheLoaiSachAdapter;
import com.example.cahngsch.Adapter.spinerAdapter;
import com.example.cahngsch.DAO.SachDAO;
import com.example.cahngsch.DAO.TheLoaiDAO;
import com.example.cahngsch.Model.Sach;
import com.example.cahngsch.Model.TheLoai;
import com.example.cahngsch.R;

import java.util.ArrayList;
import java.util.List;

public class Fragment_qlsach extends Fragment {
    SachDAO sachDAO;
    TheLoaiDAO theLoaiDAO;
    Spinner spnTheLoai;
    EditText edMaSach, edTenSach, edNXB, edTacGia, edGiaBia, edSoLuong;
    SachAdapter adapter;
    ListView lvqls;
Button btnthem;
ArrayList<TheLoai> list;
spinerAdapter spadapter;



    public Fragment_qlsach() {


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_qlsach, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
btnthem = view.findViewById(R.id.btnthemsach);
edMaSach = view.findViewById(R.id.edtmasach);
edTenSach = view.findViewById(R.id.edttensach);
edTacGia = view.findViewById(R.id.edttacgia);
edNXB = view.findViewById(R.id.edtnxb);
edGiaBia = view.findViewById(R.id.edtgiabia);
edSoLuong = view.findViewById(R.id.edtsoluong);
spnTheLoai = view.findViewById(R.id.spTheLoai);
sachDAO = new SachDAO(getContext());
theLoaiDAO = new TheLoaiDAO(getContext());
list = new ArrayList<>();
list = theLoaiDAO.getallMaLoai();
spadapter = new spinerAdapter(getContext(),list);
spnTheLoai.setAdapter(spadapter);
btnthem.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        String masach = edMaSach.getText().toString();
        String tensach = edTenSach.getText().toString();
        String tacgia = edTacGia.getText().toString();
        String nxb = edNXB.getText().toString();
        String giabia = edGiaBia.getText().toString();
        String soluong = edSoLuong.getText().toString();


    }
});





    }
}


