package com.example.cahngsch.Fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.cahngsch.Adapter.NguoiDungAdapter;
import com.example.cahngsch.ChiTietNguoiDung;
import com.example.cahngsch.DAO.NguoiDungDAO;
import com.example.cahngsch.Model.NguoiDung;
import com.example.cahngsch.R;

import java.util.ArrayList;
import java.util.List;

public class Fragment_dsnguoidung extends Fragment {
ArrayList<NguoiDung> list;
    ListView lvNguoiDung;
    NguoiDungAdapter adapter = null;
    NguoiDungDAO nguoiDungDAO;

    public Fragment_dsnguoidung(){


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_qlnguoidung,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lvNguoiDung = view.findViewById(R.id.lvqlnd);
        list = new ArrayList<>();
        nguoiDungDAO = new NguoiDungDAO(getContext());
        list= (ArrayList<NguoiDung>) nguoiDungDAO.getAllNguoiDung();
        adapter = new NguoiDungAdapter((Activity) getContext(),list);
        lvNguoiDung.setAdapter(adapter);
        FloatingActionButton floatingActionButton=getActivity().findViewById(R.id.FABThemnguoidung);
        floatingActionButton.show();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                //gắn layout vào view
                LayoutInflater inflater = getActivity().getLayoutInflater();
                v = inflater.inflate(R.layout.activity_them_nguoi_dung, null);
                alertDialog.setView(v);
                //ánh xạ
                final EditText edUsername=(EditText)v.findViewById(R.id.edUserName);
                final EditText edtPass = (EditText) v.findViewById(R.id.edPassword);
                final EditText edtRePass = (EditText) v.findViewById(R.id.edRePassword);
                final EditText edtPhone = (EditText) v.findViewById(R.id.edPhone);
                final EditText edtFullname = (EditText) v.findViewById(R.id.edFullName);
                //gắn thêm nút và hiển thị dialog
                alertDialog.setPositiveButton("Thêm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            NguoiDung nd = new NguoiDung();
                            nd.setUserName(edUsername.getText().toString());
                            nd.setPassword(edtPass.getText().toString());
                            nd.setPhone(edtPhone.getText().toString());
                            nd.setHoTen(edtFullname.getText().toString());
                            if (nguoiDungDAO.insertNguoiDung(nd) == -1) {
                                Toast.makeText(getContext(), "Thêm không thành công", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Toast.makeText(getContext(), "Thêm thành công", Toast.LENGTH_SHORT).show();
                            list = (ArrayList<NguoiDung>) nguoiDungDAO.getAllNguoiDung();
                            adapter = new NguoiDungAdapter((Activity) getContext(),list);
                            lvNguoiDung.setAdapter(adapter);
                        } catch (NullPointerException npe) {
                            Toast.makeText(getContext(), "Lỗi NullPointerException", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                alertDialog.setNegativeButton("Thoát", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }

        });

            }

}
