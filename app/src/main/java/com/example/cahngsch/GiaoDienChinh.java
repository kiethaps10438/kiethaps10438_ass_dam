package com.example.cahngsch;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.example.cahngsch.Fragment.Fragment_dsnguoidung;
import com.example.cahngsch.Fragment.Fragment_gioithieu;
import com.example.cahngsch.Fragment.Fragment_qlhoadon;
import com.example.cahngsch.Fragment.Fragment_qlsach;
import com.example.cahngsch.Fragment.Fragment_qltheloaisach;
import com.example.cahngsch.Fragment.Fragment_sachhvatheloai;
import com.example.cahngsch.Fragment.Fragment_thongke;

public class GiaoDienChinh extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giao_dien_chinh);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.giao_dien_chinh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_tnd) {
            Intent intent = new Intent();
            startActivity(intent);
            return true;
        } else if (id == R.id.action_dmk){
            Intent intent = new Intent(GiaoDienChinh.this,DoiMatKhau.class);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        int id = item.getItemId();
        if (id == R.id.nav_gioiithieu) {
            fragmentTransaction.replace(R.id.frame_layout, new Fragment_gioithieu()).commit();
        } else if (id == R.id.nav_qlhd) {
            fragmentTransaction.replace(R.id.frame_layout, new Fragment_qlhoadon()).commit();
        } else if (id == R.id.nav_qlsach) {
            fragmentTransaction.replace(R.id.frame_layout, new Fragment_sachhvatheloai()).commit();
        } else if (id == R.id.nav_qltnd) {
            fragmentTransaction.replace(R.id.frame_layout, new Fragment_dsnguoidung()).commit();
        }else if (id == R.id.nav_thongke) {
                fragmentTransaction.replace(R.id.frame_layout, new Fragment_thongke()).commit();
        } else if (id == R.id.nav_thoat) {
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
