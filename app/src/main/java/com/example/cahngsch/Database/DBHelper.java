package com.example.cahngsch.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.cahngsch.DAO.HoaDonChiTietDAO;
import com.example.cahngsch.DAO.HoaDonDAO;
import com.example.cahngsch.DAO.NguoiDungDAO;
import com.example.cahngsch.DAO.SachDAO;
import com.example.cahngsch.DAO.TheLoaiDAO;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "dbBookManager";
    public static final int VERSION = 1;
    public DBHelper (Context context){
        super(context,DATABASE_NAME,null,VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql;
        sql = "CREATE TABLE NGUOIDUNG(" +
                "USERNAME TEXT PRIMARY KEY," +
                "HOTEN TEXT," +
                "PHONE TEXT)";
        db.execSQL(sql);
sql ="CREATE TABLE THELOAI(" + "MALOAI TEXT PRIMARY KEY," +" TENLOAI TEXT,"+"MOTA TEXT)";
db.execSQL(sql);
        db.execSQL(sql);
        db.execSQL(SachDAO.SQL_SACH);
        db.execSQL(HoaDonDAO.SQL_HOA_DON);
        db.execSQL(HoaDonChiTietDAO.SQL_HOA_DON_CHI_TIET);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop table if exists "+NguoiDungDAO.TABLE_NAME);
        db.execSQL("Drop table if exists "+TheLoaiDAO.TABLE_NAME);
        db.execSQL("Drop table if exists "+SachDAO.TABLE_NAME);
        db.execSQL("Drop table if exists "+HoaDonDAO.TABLE_NAME);
        db.execSQL("Drop table if exists "+HoaDonChiTietDAO.TABLE_NAME);
        onCreate(db);

    }
}
