package com.example.cahngsch.Fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.cahngsch.Adapter.NguoiDungAdapter;
import com.example.cahngsch.Adapter.TheLoaiSachAdapter;
import com.example.cahngsch.DAO.NguoiDungDAO;
import com.example.cahngsch.DAO.TheLoaiDAO;
import com.example.cahngsch.Model.NguoiDung;
import com.example.cahngsch.Model.TheLoai;
import com.example.cahngsch.R;

import java.util.ArrayList;
import java.util.List;

public class Fragment_qltheloaisach extends Fragment {
    ArrayList<TheLoai> list;
    ListView lvTheLoai;
    TheLoaiSachAdapter adapter = null;
    TheLoaiDAO theloaDAO;

    public Fragment_qltheloaisach(){


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_qltheloaisach,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lvTheLoai = view.findViewById(R.id.lvqltls);
        registerForContextMenu(lvTheLoai);
        theloaDAO = new TheLoaiDAO(getContext());
        list = (ArrayList<TheLoai>) theloaDAO.getAllTheLoai();
        adapter = new TheLoaiSachAdapter((Activity) getContext(),list);
        lvTheLoai.setAdapter(adapter);

        // float
        FloatingActionButton floatingActionButton=getActivity().findViewById(R.id.FABtls);
        floatingActionButton.show();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                //gắn layout vào view
                LayoutInflater inflater = getActivity().getLayoutInflater();
                v = inflater.inflate(R.layout.themtheloaisach, null);
                alertDialog.setView(v);
                //ánh xạ
                final EditText edtMaloai=(EditText)v.findViewById(R.id.edtmatheloai);
                final EditText edtTenloai = (EditText) v.findViewById(R.id.edttentheloai);
                final EditText edtmota = (EditText) v.findViewById(R.id.edtmota);
                final EditText edtvitri = (EditText) v.findViewById(R.id.edtvitri);
                //gắn thêm nút và hiển thị dialog
                alertDialog.setPositiveButton("Thêm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            TheLoai theLoai = new TheLoai();
                            theLoai.setMaTheLoai(edtMaloai.getText().toString());
                            theLoai.setTenTheLoai(edtTenloai.getText().toString());
                            theLoai.setMoTa(edtmota.getText().toString());
                            theLoai.setViTri(Integer.parseInt(edtvitri.getText().toString()));
                            if (theloaDAO.inserTheLoai(theLoai) == -1) {
                                Toast.makeText(getContext(), "Thêm không thành công", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Toast.makeText(getContext(), "Thêm thành công", Toast.LENGTH_SHORT).show();
                            list = (ArrayList<TheLoai>) theloaDAO.getAllTheLoai();
                            TheLoaiSachAdapter adapter = new TheLoaiSachAdapter((Activity) getContext(), list);
                            lvTheLoai.setAdapter(adapter);
                        } catch (NullPointerException npe) {
                            Toast.makeText(getContext(), "Lỗi NullPointerException", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                alertDialog.setNegativeButton("Thoát", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });

// the hien chi tiet the loai
        lvTheLoai.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent intent = new
                        Intent();
                Bundle b = new Bundle();
                b.putString("MATHELOAI", list.get(position).getMaTheLoai());
                b.putString("TENTHELOAI", list.get(position).getTenTheLoai());
                b.putString("MOTA", list.get(position).getMoTa());
                b.putString("VITRI",
                        String.valueOf(list.get(position).getViTri()));
                intent.putExtras(b);
                startActivity(intent);
            }
        });
    }

    }

