package com.example.cahngsch.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.cahngsch.DAO.SachDAO;
import com.example.cahngsch.Model.Sach;
import com.example.cahngsch.Model.TheLoai;
import com.example.cahngsch.R;

import java.util.ArrayList;
import java.util.List;

public class spinerAdapter extends BaseAdapter {
    Context context;
    ArrayList<TheLoai> list2;
    SachDAO sachDAO;


    public spinerAdapter(Context context, ArrayList<TheLoai> list2) {
        this.context = context;
        sachDAO = new SachDAO(context);
        this.list2 = list2;
    }


    @Override
    public int getCount() {
        return list2.size();
    }

    @Override
    public Object getItem(int position) {
        return list2.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);// cho get co 2 lua chon
            convertView=layoutInflater.inflate(R.layout.spinlop,null);
        }

        TextView tvspinmasach=convertView.findViewById(R.id.tvspinmasach);


        tvspinmasach.setText(list2.get(position).getMaTheLoai());
        // bo sung them cho dep de hieu

        return convertView;
    }

}
