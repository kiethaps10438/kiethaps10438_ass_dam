package com.example.cahngsch.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cahngsch.DAO.NguoiDungDAO;
import com.example.cahngsch.Model.NguoiDung;
import com.example.cahngsch.R;

import java.util.List;

public class NguoiDungAdapter extends BaseAdapter {
    List<NguoiDung> arrNguoiDung;
    public Activity context;
    public LayoutInflater inflater;
    NguoiDungDAO nguoiDungDAO;

    public NguoiDungAdapter(Activity context, List<NguoiDung> arrayNguoiDung) {
        super();
        this.context = context;
        this.arrNguoiDung = arrayNguoiDung;
        this.inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        nguoiDungDAO = new NguoiDungDAO(context);
    }

    @Override
    public int getCount() {
        return arrNguoiDung.size();
    }

    @Override
    public Object getItem(int position) {
        return arrNguoiDung.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        ImageView img;
        TextView txtName;
        TextView txtPhone;
        ImageView imgDelete;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_nguoi_dung, null);
            holder.img = (ImageView) convertView.findViewById(R.id.ivIcon);
            holder.txtName = (TextView) convertView.findViewById(R.id.tvName);
            holder.txtPhone = (TextView) convertView.findViewById(R.id.tvPhone);
            holder.imgDelete = (ImageView) convertView.findViewById(R.id.ivDelete);
            holder.imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Snackbar.make(((Activity) context).findViewById(R.id.layout_qlnd), "Bạn có muốn xóa", 5000)
                            .setActionTextColor(Color.GREEN)
                            .setAction("yes", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    NguoiDung nd = arrNguoiDung.get(position);
                                    nguoiDungDAO = new NguoiDungDAO(context);
                                    if (nguoiDungDAO.deleteNguoiDungByID(nd) == -1) {
                                        Toast.makeText(context, "Xóa không thành công", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    Toast.makeText(context, "Xóa thành công", Toast.LENGTH_SHORT).show();
                                    arrNguoiDung.clear();
                                    arrNguoiDung.addAll(nguoiDungDAO.getAllNguoiDung());
                                    NguoiDungAdapter.this.notifyDataSetChanged();
                                }
                            })
                            .show();
                }
            });}
 else {
                holder = (ViewHolder) convertView.getTag();
            }
            NguoiDung nguoiDung = arrNguoiDung.get(position);
            holder.txtName.setText(nguoiDung.getHoTen());
            return convertView;
        }

    }


